import sys

from enum import Enum


class TokenType(Enum):
    BOOLEAN = -2
    STRING = -1

    EQUAL = 0
    UNEQUAL = 1
    NOT = 2
    OR = 3
    AND = 4

    EOF = 5

    LPAREN = 6
    RPAREN = 7


class Token:
    def __init__(self, type, value=None):
        self.type = type
        self.value = value

    def __str__(self):
        return str(self.type) + " " + str(self.value)


class Lexer:
    def __init__(self, str_input):
        self.position = 0
        self.tokens = list()
        self.code = str_input
        self.length = len(str_input)
        self.operators = ["(", ")", "="]
    
    def tokenize(self):
        while self.position < self.length:
            current = self.peek(0)
            if current == "\"":
                self.position += 1
                self.tokenize_string()
            elif current.isalpha():
                self.tokenize_word()
            elif current == " ":
                self.position += 1
            elif current in self.operators:
                self.tokenize_operator()
            else: 
                self.position += 1
        return self.tokens

    def peek(self, rel_position):
        return self.code[self.position + rel_position]

    def tokenize_string(self):
        string = ""
        pos = 0
        current = self.peek(pos)
        while not current == "\"":
            string += current
            pos += 1
            if self.position + pos >= self.length:
                print("Expecting \" on " + str(self.length))
                sys.exit(65)
            current = self.peek(pos)
        self.tokens.append(Token(TokenType.STRING, string))
        self.position += pos + 1

    def tokenize_word(self):
        string = ""
        current = self.peek(0)
        while current.isalnum() or current == "_":
            string += current
            self.position += 1
            if self.position >= self.length:
                break
            current = self.peek(0)
        if string == "not":
            self.tokens.append(Token(TokenType.NOT))
        elif string == "and":
            self.tokens.append(Token(TokenType.AND))
        elif string == "or":
            self.tokens.append(Token(TokenType.OR))
        elif string == "true":
            self.tokens.append(Token(TokenType.BOOLEAN, True))
        elif string == "false":
            self.tokens.append(Token(TokenType.BOOLEAN, False))
        else:
            raise Exception("Unknown word.")

    def tokenize_operator(self):
        current = self.peek(0)
        if current == "=":
            if self.position + 1 < self.length:
                if self.peek(1) == "=":
                    self.tokens.append(Token(TokenType.EQUAL))
                    self.position += 2
                else:
                    raise Exception("Unsupported operator")
            else:
                raise Exception("Unsupported operator =")
        elif current == "(":
            self.tokens.append(Token(TokenType.LPAREN))
            self.position += 1
        elif current == ")":
            self.tokens.append(Token(TokenType.RPAREN))
            self.position += 1
        else:
            raise Exception("Unsupported operator")


class BooleanExpression:
    def __init__(self, value):
        self.value = value

    def eval(self):
        return self.value

    def __str__(self):
        return str(self.value)


class StringExpression:
    def __init__(self, value):
        self.value = value

    def eval(self):
        return self.value

    def __str__(self):
        return self.value


class UnaryExpression:
    def __init__(self, expr1, operation):
        self.expr1 = expr1
        self.operation = operation

    def eval(self):
        if self.operation == TokenType.NOT:
            #print("not", self.expr1.eval(), "=", not self.expr1.eval())
            return BooleanExpression(not self.expr1.eval())
            
    def __str__(self):
        if self.operation == TokenType.NOT:
            return "not " + str(self.expr1)


class BinaryExpression:
    def __init__(self, expr1, expr2, operation):
        self.expr1 = expr1
        self.expr2 = expr2
        self.operation = operation

    def eval(self):
        if self.operation == TokenType.EQUAL:
            return BooleanExpression(self.expr1.eval() == self.expr2.eval())
        elif self.operation == TokenType.AND:
            return BooleanExpression(self.expr1.eval() and self.expr2.eval())
        elif self.operation == TokenType.OR:
            return BooleanExpression(self.expr1.eval() or self.expr2.eval())

    def __str__(self):
        if self.operation == TokenType.EQUAL:
            return str(self.expr1) + "==" + str(self.expr2)
        if self.operation == TokenType.AND:
            return str(self.expr1) + "and" + str(self.expr2)
        if self.operation == TokenType.OR:
            return str(self.expr1) + "or" + str(self.expr2)


class Parser:
    def __init__(self, tokens_list):
        self.position = 0
        self.tokens = tokens_list
        self.length = len(tokens_list)
        self.eof = Token(TokenType.EOF)

    def peek(self, rel_position):
        position = self.position + rel_position
        if position >= self.length:
            return self.eof
        return self.tokens[position]

    def match(self, type):
        if self.peek(0).type == type:
            self.position += 1
            return True
        return False

    def parse(self):
        result = list()
        while not self.match(TokenType.EOF):
            result.append(self.expression())
            return result

    def expression(self):
        return self.additive()

    def additive(self):
        result = self.multiplicative()
        while True:
            if self.match(TokenType.OR):
                result = BinaryExpression(result, self.multiplicative(), TokenType.OR)
                continue
            break
        return result

    def multiplicative(self):
        result = self.equivalence()
        while True:
            if self.match(TokenType.AND):
                #print("eval AND")
                result = BinaryExpression(result, self.equivalence(), TokenType.AND)
                continue
            break
        return result

    def equivalence(self):
        result = self.unary()
        while True:
            if self.match(TokenType.EQUAL):
                #print("eval EQUAL")
                result = BinaryExpression(result, self.unary(), TokenType.EQUAL)
                continue
            break
        return result

    def unary(self):
        if self.match(TokenType.NOT):
            #print("eval NOT")
            return UnaryExpression(self.primary(), TokenType.NOT)
        return self.primary()

    def primary(self):
        current = self.peek(0)
        if self.match(TokenType.STRING):
            return StringExpression(current.value)
        elif self.match(TokenType.BOOLEAN):
            return BooleanExpression(current.value)
        elif self.match(TokenType.LPAREN):
            #print("eval (")
            result = self.expression()
            #print("eval )")
            self.match(TokenType.RPAREN)
            return result
        else:
            raise Exception("Unknown expression")


def eval(expr):
    l = Lexer(expr)
    token_list = l.tokenize()
    parsed_list = Parser(token_list).parse()
    return parsed_list[0].eval()


if __name__ == "__main__":
    print(eval(input("Введите выражение:")))