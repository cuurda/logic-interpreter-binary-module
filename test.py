import unittest
import qd_evaluator


class TestQDEvaluator(unittest.TestCase):
    def test_true(self):
        self.assertEqual(qd_evaluator.eval("true"), True)

    def test_false(self):
        self.assertEqual(qd_evaluator.eval("false"), False)

    def test_equal(self):
        self.assertEqual(qd_evaluator.eval("\"vasabi\" == \"vasabi\""), True)
        self.assertEqual(qd_evaluator.eval("\"parrot\" == \"vasabi\""), False)

    def test_not(self):
        self.assertEqual(qd_evaluator.eval("not false"), True)
        self.assertEqual(qd_evaluator.eval("not true"), False)

    def test_or(self):
        self.assertEqual(qd_evaluator.eval("false or false"), False)
        self.assertEqual(qd_evaluator.eval("false or true"), True)
        self.assertEqual(qd_evaluator.eval("true or false"), True)
        self.assertEqual(qd_evaluator.eval("true or true"), True)

    def test_and(self):
        self.assertEqual(qd_evaluator.eval("false and false"), False)
        self.assertEqual(qd_evaluator.eval("false and true"), False)
        self.assertEqual(qd_evaluator.eval("true and false"), False)
        self.assertEqual(qd_evaluator.eval("true and true"), True)

    def test_paren(self):
        self.assertEqual(qd_evaluator.eval("false and (false or false)"), False)
        self.assertEqual(qd_evaluator.eval("false and (false or true)"), False)
        self.assertEqual(qd_evaluator.eval("false and (true or false)"), False)
        self.assertEqual(qd_evaluator.eval("false and (true or true)"), False)
        self.assertEqual(qd_evaluator.eval("true and (false or false)"), False)
        self.assertEqual(qd_evaluator.eval("true and (false or true)"), True)
        self.assertEqual(qd_evaluator.eval("true and (true or false)"), True)
        self.assertEqual(qd_evaluator.eval("true and (true or true)"), True)

    def test_exception(self):
        with self.assertRaises(ValueError) as context:
            qd_evaluator.eval("fals")
        self.assertEqual(str(context.exception), "Wrong operator on 4")
        with self.assertRaises(ValueError) as context:
            qd_evaluator.eval("tru")
        self.assertEqual(str(context.exception), "Wrong operator on 3")
        with self.assertRaises(ValueError) as context:
            qd_evaluator.eval("no")
        self.assertEqual(str(context.exception), "Wrong operator on 2")
        with self.assertRaises(ValueError) as context:
            qd_evaluator.eval("\"rehwaaegw\" == \"ewas")
        self.assertEqual(str(context.exception), "Expecting \" on 20")
        with self.assertRaises(ValueError) as context:
            qd_evaluator.eval("\"rehwaaegw\"=\"ewas\"")
        self.assertEqual(str(context.exception), "Wrong operator on 11")
        with self.assertRaises(ValueError) as context:
            qd_evaluator.eval("\"rehwaaegw\"=")
        self.assertEqual(str(context.exception), "Wrong operator = on 11")


if __name__ == "__main__":
    unittest.main()