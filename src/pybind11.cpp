#include <iostream>
#include <pybind11/pybind11.h>

#include <string>
#include <vector>
#include "Lexer.h" 
#include "Token.h"
#include "Parser.h"
#include "ast/Expression.h"

using namespace std;
namespace py = pybind11;

/*int add(int i, int j) {
    return i + j;
}

void prints(const std::string &s){
    std::cout<<s<<"\n";
}
bool print(const std::string &s){
    std::cout<<"Hi from C++\n";
    prints(s);
    return true;
}*/

/* Do evaluation of (one) boolean expression
    Args:
        code: code for evaluation
*/
bool eval(const std::string &code){
    Lexer lexer(code);
    std::vector<Token> tokens = lexer.tokenize();
    Parser parser(tokens);
    std::vector<Expression*> expressions = parser.parse();
    return expressions[0]->eval()->asBoolean();
}
PYBIND11_MODULE(qd_evaluator, m) {
    m.doc() = "pybind11 example plugin"; // optional module docstring

    //m.def("add", &add, "A function which adds two numbers");
    //m.def("cprint", &print, "A function which prints");
    m.def("eval", &eval, "A function which evaluates boolean expression");
    
}