#include <cctype>
#include <cstdlib>
#include <stdexcept>
#include <string>
#include <vector>

#include "Token.h"

using namespace std;
class Lexer {
    public:
        Lexer(const string& code);
        vector<Token> tokenize();
    private:
        vector<Token> mTokens;
        string mCode;
        int mTokensVolume;
        int mTokensSize;
        int mPositon;
        int mLenght;
        
        void addToken(Token token);
        char& peek(int relativePosition);
        void tokenizeString();
        void tokenizeOperator(); 
        void tokenizeWord();
        static bool isOperator(char& forCheking);
};