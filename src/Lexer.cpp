#include "Lexer.h"

Lexer::Lexer(const string& code){
    this->mCode = code;
    this->mTokensVolume = 4;
    this->mTokensSize = 0;
    this->mPositon = 0;
    this->mLenght = code.size();
    this->mTokens.reserve(this->mTokensVolume);
}

//Tokenize input string, passed to constructor by reference
vector<Token> Lexer::tokenize(){
    while (this->mPositon < this->mLenght){
        char& current = this->peek(0);
        if (current == '"'){
            this->mPositon++;
            this->tokenizeString();
        }
        else if (isalpha(current)){
            this->tokenizeWord();
        }
        else if (this->isOperator(current)){
            this->tokenizeOperator();
        }
        else{
            this->mPositon++;
        }
    }
    return this->mTokens;
}

/*Tokenize string
  after quote
  till closing quote
  or throw exception if no closing quote*/
void Lexer::tokenizeString(){
    string str;
    str.reserve(8);
    int pos = 0, volume = 8;
    char& current = this->peek(0);
    while (current != '"'){
        if (pos + 1 >= volume){
            volume *= 2;
            str.reserve(volume);
        }
        str += current;
        pos++;
        if (this->mPositon + pos >= this->mLenght){
            throw invalid_argument("Expecting \" on " + to_string(this->mLenght));
        }
        current = this->peek(pos);
    }
    this->addToken(Token(TokenType::STRING, str));
    this->mPositon += pos + 1; 
}

/*Tokenize word if it is begin with letter & registered
  or throws exception if word is not registered*/
void Lexer::tokenizeWord(){
    string str;
    str.reserve(8);
    int volume = 8, lenght = 0, pos = 0;
    char& current = this->peek(pos);
    while (isalnum(current) || current == '_'){
        if (lenght + 1 >= volume){
            volume *= 2;
            str.reserve(volume);
        }
        str += current;
        this->mPositon++;
        lenght++;
        if (this->mPositon + pos >= this->mLenght){
            break;
        }
        current = this->peek(pos);
    }
    if (str == "not"){
        this->addToken(Token(TokenType::NOT));
    }
    else if(str == "and"){
        this->addToken(Token(TokenType::AND));
    }
    else if(str == "or"){
        this->addToken(Token(TokenType::OR));
    }
    else if(str == "true" || str == "false"){
        this->addToken(Token(TokenType::BOOLEAN, str));
    }
    else{
        throw invalid_argument("Wrong operator on " + to_string(this->mPositon));
    }
}

/*Tokenize operator if it is registered
  or throws exception if not*/
void Lexer::tokenizeOperator(){
    char& current = this->peek(0);
    if (current == '='){
        if (this->mPositon + 1 < this->mLenght){
            if(this->peek(1) == '='){
                this->addToken(Token(TokenType::EQUAL));
                this->mPositon += 2;
            }
            else{
                throw invalid_argument("Wrong operator on " + to_string(this->mPositon));
            }
        }else{
            throw invalid_argument("Wrong operator = on " + to_string(this->mPositon));
        }
    }
    else if (current == '('){
        this->addToken(Token(TokenType::LPAREN));
        this->mPositon++;
    }
    else if (current == ')'){
        this->addToken(Token(TokenType::RPAREN));
        this->mPositon++;
    }
    else {
        throw invalid_argument("Wrong operator on " + to_string(this->mPositon));
    }
}

/*Peek symbol at relative to current position in code
    Args:
        relativePosition: delta to the next symbol.
            0 is current symbol, 1 is next symbol, 2 is followed by next symbol etc.
    Return:
        Char symbol by reference at selected position.
*/
char& Lexer::peek(int relativePosition){
    return this->mCode.at(this->mPositon + relativePosition);
}

/*Check if passed symbol operator or begins of operator
    Args:
        forChecking: Reference to symbol for check.
    Return:
        Is passed symbol operator or begins of operator.*/
bool Lexer::isOperator(char& forCheking){
    return forCheking == '=' ||
        forCheking == '(' ||
        forCheking == ')';
}

/*Add token to array of tokens with multiplicative extending
it if needs.
    Args:
        token: Token for adding by value*/
void Lexer::addToken(Token token){
    if (this->mTokensSize + 1 >= this->mTokensVolume){
        this->mTokensVolume *= 2;
        this->mTokens.reserve(this->mTokensVolume);
    }
    this->mTokens.push_back(token);
}
