#include "Parser.h"

const Token Parser::EOFILE = Token(TokenType::EOFILE);

Parser::Parser(vector<Token> tokens){
    this->mTokens = tokens;
    this->mTokensLenght = tokens.size();
    this->mPositon = 0;
    
    this->mExpressionsSize = 0;
    this->mExpressionsVolume = 4;
    this->mExpressions.reserve(this->mExpressionsVolume);
}

/*Peek token at relative to current position in array.
    Args:
        relativePosition: delta to the next token.
            0 is current token, 1 is next token, 2 is followed by next token etc.
    Return:
        Token by value at selected position.
*/
Token Parser::peek(int relativePosition){
    int position = this->mPositon + relativePosition;
    if (position >= this->mTokensLenght) return this->EOFILE;
    return this->mTokens[position];
}

/*Check if token type predicted correctly & move position
    Args:
        type: predicted type of token
    Return:
        is token type predicted correctly?
*/
bool Parser::match(TokenType::TokenType type){
    if (peek(0).getType() == type){
        this->mPositon++;
        //std::cout<<type<<"\n";
        return true;
    }
    return false;
}

/*Parse tokens to tree of expressions
    Return:
        Vector of trees of expressions
*/
vector<Expression*> Parser::parse(){
    while (!this->match(TokenType::EOFILE)){
        this->addExpression(this->expression());
    }
    return this->mExpressions;
}

/*Main expressions recursive layer
    Return:
        Tree of expressions
*/
Expression* Parser::expression(){
    return this->additive();
}

/*Additive expressions recursive layer.
  It handles "or" operator.
    Return:
        Tree of expression
*/
Expression* Parser::additive(){
    Expression* result = this->multiplicative();
    while (true){
        if (this->match(TokenType::OR)){
            result = new BinaryExpression(TokenType::OR, result, this->multiplicative());
            continue;
        }
        break;
    }
    return result;
}

/*Multiplicative expressions recursive layer.
  It handles "and" operator.
    Return:
        Tree of expression
*/
Expression* Parser::multiplicative(){
    Expression* result = this->equivalence();
    while (true){
        if (this->match(TokenType::AND)){
            result = new BinaryExpression(TokenType::AND, result, this->equivalence());
            continue;
        }
        break;
    }
    return result;
}

/*Equivalence expressions recursive layer.
  It handles "==" operator.
    Return:
        Tree of expression
*/
Expression* Parser::equivalence(){
    Expression* result = this->unary();
    while (true){
        if (this->match(TokenType::EQUAL)){
            result = new BinaryExpression(TokenType::EQUAL, result, this->unary());
            continue;
        }
        break;
    }
    return result;
}

/*Unary expressions recursive layer.
  It handles "not" operator.
    Return:
        Tree of expression
*/
Expression* Parser::unary(){
    if (this->match(TokenType::NOT)){
        return new UnaryExpression(TokenType::NOT, this->primary());
    }
    return this->primary();
}

/*Primary expressions recursive layer.
  It handles string or boolean value or expressions in paren.
    Return:
        Tree of expression
*/
Expression* Parser::primary(){
    Token current = this->peek(0);
    if (this->match(TokenType::STRING)){
        return new StringExpression(current.getValue());
    }
    else if (this->match(TokenType::BOOLEAN)){
        if (current.getValue() == "true"){
            return new BooleanExpression(true);
        }
        else{
            return new BooleanExpression(false);
        }
    }
    else if (this->match(TokenType::LPAREN)){
        Expression* result = this->expression();
        this->match(TokenType::RPAREN);
        return result;
    }
    else {
        throw invalid_argument("Unknown token.");
    }
}

/*Add expression to array of expressions with multiplicative extending
it if needs.
    Args:
        token: Token for adding by value*/
void Parser::addExpression(Expression* e){
    if (this->mExpressionsSize + 1 >= this->mExpressionsVolume){
        this->mExpressionsVolume *= 2;
        this->mExpressions.reserve(this->mExpressionsVolume);
    }
    this->mExpressions.push_back(e);
}