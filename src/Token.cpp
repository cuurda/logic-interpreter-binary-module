#include "Token.h"

Token::Token(TokenType::TokenType type){
    this->mType = type;
    this->mValue = ""; 
}

Token::Token(TokenType::TokenType type, string value){
    this->mType = type;
    this->mValue = value;
}

/*getter for type */
TokenType::TokenType Token::getType(){
    return this->mType;
}

/*getter for value */
string Token::getValue(){
    return this->mValue;
}
