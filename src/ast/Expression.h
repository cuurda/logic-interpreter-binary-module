#ifndef EXPRESSION_H
#define EXPRESSION_H

#include "../lib/Value.h"

class Expression {
    public:
        virtual Value* eval() = 0;
};

#endif //EXPRESSION_H