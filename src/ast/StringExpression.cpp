#include "StringExpression.h"

StringExpression::StringExpression(string value){
    this->mValue = new StringValue(value);
}

Value* StringExpression::eval(){
    return this->mValue;
}