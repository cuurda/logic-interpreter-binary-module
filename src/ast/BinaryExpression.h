#ifndef BINARYEXPRESSION_H
#define BINARYEXPRESSION_H
#include "../TokenType.cpp"
#include "Expression.h"
#include "../lib/BooleanValue.h"

class BinaryExpression : public Expression
{
public:
    BinaryExpression(TokenType::TokenType operation, Expression* expression1, Expression* expression2);
    ~BinaryExpression();
    Value* eval();
private:
    Expression* expr1;
    Expression* expr2;
    TokenType::TokenType operation;
};

#endif // BINARYEXPRESSION_H
