#ifndef STRINGEXPRESSION_H
#define STRINGEXPRESSION_H
#include <string>
#include "../lib/Value.h"
#include "../lib/StringValue.h"
#include "Expression.h"

class StringExpression : public Expression
{
    public:
        StringExpression(string value);
        Value* eval();
    private:
        StringValue* mValue;
};

#endif // STRINGEXPRESSION_H
