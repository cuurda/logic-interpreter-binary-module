#ifndef UNARYEXPRESSION_H
#define UNARYEXPRESSION_H

#include "../TokenType.cpp"
#include "Expression.h" // Base class: Expression
#include "../lib/BooleanValue.h"

class UnaryExpression : public Expression
{
public:
    UnaryExpression(TokenType::TokenType operation, Expression* expression);
    ~UnaryExpression();
    Value* eval();
    
private:
    TokenType::TokenType operation;
    Expression* expression;
};

#endif // UNARYEXPRESSION_H
