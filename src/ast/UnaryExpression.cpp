#include "UnaryExpression.h"

UnaryExpression::UnaryExpression(TokenType::TokenType operation, Expression* expression)
{
    this->operation = operation;
    this->expression = expression;
}

UnaryExpression::~UnaryExpression()
{
}

Value* UnaryExpression::eval()
{
    if (this->operation == TokenType::NOT){
        return new BooleanValue(this->expression->eval()->not_());
    }
    throw "Unknown Unary Expression";
}
