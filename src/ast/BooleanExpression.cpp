#include "BooleanExpression.h"

BooleanExpression::BooleanExpression(bool value)
{
    this->value = new BooleanValue(value);
}

Value* BooleanExpression::eval(){
    return this->value;
}