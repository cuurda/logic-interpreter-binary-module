#include "BinaryExpression.h"

BinaryExpression::BinaryExpression(TokenType::TokenType operation, Expression* expression1, Expression* expression2)
{
    this->operation = operation;
    this->expr1 = expression1;
    this->expr2 = expression2;
}

BinaryExpression::~BinaryExpression()
{
}

Value* BinaryExpression::eval()
{
    switch (this->operation){
        case TokenType::EQUAL:
           return new BooleanValue(this->expr1->eval()->equal(this->expr2->eval()));
        case TokenType::AND:
            return new BooleanValue(this->expr1->eval()->and_(this->expr2->eval()));
        case TokenType::OR:
            return new BooleanValue(this->expr1->eval()->or_(this->expr2->eval()));
    }
    throw "Unknown Binary Expression";
}
