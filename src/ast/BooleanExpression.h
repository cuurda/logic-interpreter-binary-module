#ifndef BOOLEANEXPRESSION_H
#define BOOLEANEXPRESSION_H
#include "Expression.h"
#include "../lib/Value.h"
#include "../lib/BooleanValue.h"

class BooleanExpression : public Expression
{
public:
    BooleanExpression(bool value);
    Value* eval(); 
private:
    Value* value;
};

#endif // BOOLEANEXPRESSION_H
