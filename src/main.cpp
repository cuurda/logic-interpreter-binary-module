/*
 * main.cpp
 * 
 * Copyright 2017 Никита Левонович <cuurda@MacBook-Pro-Nikita.local>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include <iostream>
#include <string>
#include <vector>
#include "Lexer.h"
#include "Token.h"
#include "Parser.h"
#include "ast/Expression.h"

void printTokens(vector<Token> tokens){
    for (int i = 0; i<tokens.size(); i++){
        std::cout<<tokens[i].getType()<<"\t"<<tokens[i].getValue()<<"\n";
    }
}

void printExpressions(vector<Expression*> expressions){
    for (int i = 0; i<expressions.size(); i++){
        std::cout<<expressions[i]->eval()->asString()<<"\n";
    }
}
int main(int argc, char **argv)
{
    std::string code;
    std::getline(cin, code);
    std::cout<<code<<"\n";
    Lexer lexer(code);
    vector<Token> tokens = lexer.tokenize();
    printTokens(tokens);
    std::cout<<"Expressions:\n";
    Parser parser(tokens);
    vector<Expression*> expressions = parser.parse();
    printExpressions(expressions);
    std::cout<<expressions[0]->eval();
	return 0;
}

