#include <vector>
#include <stdexcept>
#include <iostream>
#include "Token.h"
#include "TokenType.cpp"
#include "ast/Expression.h"
#include "ast/BinaryExpression.h"
#include "ast/UnaryExpression.h"
#include "ast/StringExpression.h"
#include "ast/BooleanExpression.h"

using namespace std;

class Parser{
    public:
        Parser(vector<Token> tokens);
        vector<Expression*> parse();
    private:
        vector<Token> mTokens;
        int mTokensLenght;
        int mPositon;
        
        vector<Expression*> mExpressions;
        int mExpressionsVolume;
        int mExpressionsSize;

        const static Token EOFILE; 
        
        Token peek(int relativePosition);
        bool match(TokenType::TokenType type);
        
        Expression* expression();
        Expression* additive();
        Expression* multiplicative();
        Expression* equivalence();
        Expression* unary();
        Expression* primary();
        
        void addExpression(Expression* e);
};
