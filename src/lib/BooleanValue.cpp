#include "BooleanValue.h"

BooleanValue::BooleanValue(bool value){
    this->value = value;
}

bool BooleanValue::asBoolean(){
    return this->value;
}

string BooleanValue::asString(){
    if (this->value){
        return "true";
    }
    else {
        return "false";
    }
}

BooleanValue::~BooleanValue()
{
}

bool BooleanValue::not_(){
    return !this->asBoolean();
}

bool BooleanValue::equal(Value* value){
    BooleanValue* bool_value = dynamic_cast<BooleanValue*>(value);
    if (bool_value){
        return this->value == bool_value->value;
    }
    return false;   
}

bool BooleanValue::and_(Value* value){
    BooleanValue* bool_value = dynamic_cast<BooleanValue*>(value);
    if (bool_value){
        return this->value && bool_value->value;
    }
    throw "Unknown operation and for String";
}

bool BooleanValue::or_(Value* value){
    BooleanValue* bool_value = dynamic_cast<BooleanValue*>(value);
    if (bool_value){
        return this->value || bool_value->value;
    }
    throw "Unknown operation or for String";
}