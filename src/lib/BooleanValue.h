#ifndef BOOLEANVALUE_H
#define BOOLEANVALUE_H
#include "Value.h"

class BooleanValue : public Value
{
public:
    BooleanValue(bool value);
    ~BooleanValue();
    bool asBoolean();
    string asString();
    bool not_();
    bool equal(Value* value);    
    bool and_(Value* value);
    bool or_(Value* value);
private:
    bool value;
};

#endif // BOOLEANVALUE_H
