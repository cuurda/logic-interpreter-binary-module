#ifndef VALUE_H
#define VALUE_H

#include <string>
using namespace std;

class Value {
    public:
        /* Implements conversion to boolean  */
        virtual bool asBoolean() = 0;
        /* Implements conversion to string */
        virtual string asString() = 0;
        /* Implements "not" operator */
        virtual bool not_() = 0;
        /* Implements "==" operator */
        virtual bool equal(Value* value) = 0;
        /* Implements "and" operator */
        virtual bool and_(Value* value) = 0;
        /* Implements "or" operator */
        virtual bool or_(Value* value) = 0;
};

#endif //VALUE_H