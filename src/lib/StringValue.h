#ifndef STRINGVALUE_H
#define STRINGVALUE_H

#include <string>
#include "Value.h"
#include "BooleanValue.h"

class StringValue : public Value
{
public:
    StringValue(string value);
    ~StringValue();
    bool asBoolean();
    string asString();
    bool not_();
    bool equal(Value* value);
    bool and_(Value* value);
    bool or_(Value* value);
private:
    string value;
};

#endif // STRINGVALUE_H
