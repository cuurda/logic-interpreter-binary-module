#include "StringValue.h"

StringValue::StringValue(string value)
{
    this->value = value;
}

StringValue::~StringValue()
{
}

string StringValue::asString(){
    return this->value;
}

bool StringValue::asBoolean(){
    if (this->value == ""){
        return false;
    }
    return true;
}

bool StringValue::not_(){
    return !this->asBoolean();
}

bool StringValue::equal(Value* value){
    StringValue* string_value = dynamic_cast<StringValue*>(value);
    if (string_value){
        return this->value == string_value->value;
    }
    return false;
}

bool StringValue::and_(Value* value){
    throw "Unknown operation and for String";
}

bool StringValue::or_(Value* value){
    throw "Unknown operation or for String";
}