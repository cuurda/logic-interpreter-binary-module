#ifndef TOKEN_H
#define TOKEN_H

#include <string>

#include "TokenType.cpp"

using namespace std;
class Token{
    public:
        Token(TokenType::TokenType type);
        Token(TokenType::TokenType type, string value);
        TokenType::TokenType getType();
        string getValue();
    private:
        TokenType::TokenType mType;
        string mValue;
};

#endif //TOKEN_H