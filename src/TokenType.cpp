#ifndef TOKENTYPE_H
#define TOKENTYPE_H

namespace TokenType{
    enum TokenType{
        BOOLEAN,
        STRING,
        
        EQUAL,
        NOT,
        AND,
        OR,
        
        EOFILE,
        
        LPAREN,
        RPAREN
    };
};

#endif //TOKENTYPE_H