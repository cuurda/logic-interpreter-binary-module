#include <iostream>

#include <string>
#include <vector>
#include "Lexer.h" 
#include "Token.h"
#include "Parser.h"
#include "ast/Expression.h"

using namespace std;

bool eval(const std::string &code){
    Lexer lexer(code);
    std::vector<Token> tokens = lexer.tokenize();
    Parser parser(tokens);
    std::vector<Expression*> expressions = parser.parse();
    return expressions[0]->eval()->asBoolean();
}

class MyTestSuite : public CxxTest::TestSuite
{
   public:
       void testTrue( void ){
           TS_ASSERT_EQUALS(eval("true"), true);
       }
       void testFalse( void ){
           TS_ASSERT_EQUALS(eval("false"), false);
       }
       void testEquals( void ){
           TS_ASSERT_EQUALS(eval("\"vasabi\" == \"vasabi\""), true);
           TS_ASSERT_EQUALS(eval("\"parrot\" == \"vasabi\""), false);
       }
       void testNot( void ){
           TS_ASSERT_EQUALS(eval("not false"), true);
           TS_ASSERT_EQUALS(eval("not true"), false);
       }
       void testOr( void ){
           TS_ASSERT_EQUALS(eval("false or false"), false);
           TS_ASSERT_EQUALS(eval("false or true"), true);
           TS_ASSERT_EQUALS(eval("true or false"), true);
           TS_ASSERT_EQUALS(eval("true or true"), true);
       }
       void testAnd( void ){
           TS_ASSERT_EQUALS(eval("false and false"), false);
           TS_ASSERT_EQUALS(eval("false and true"), false);
           TS_ASSERT_EQUALS(eval("true and false"), false);
           TS_ASSERT_EQUALS(eval("true and true"), true);
       }
       void testParen( void ){
           TS_ASSERT_EQUALS(eval("false and (false or false)"), false);
           TS_ASSERT_EQUALS(eval("false and (false or true)"), false);
           TS_ASSERT_EQUALS(eval("false and (true or false)"), false);
           TS_ASSERT_EQUALS(eval("false and (true or true)"), false);
           TS_ASSERT_EQUALS(eval("true and (false or false)"), false);
           TS_ASSERT_EQUALS(eval("true and (false or true)"), true);
           TS_ASSERT_EQUALS(eval("true and (true or false)"), true);
           TS_ASSERT_EQUALS(eval("true and (true or true)"), true);
       }

       void testExceptions( void ){
           TS_ASSERT_THROWS_EQUALS(eval("fals"), const std::invalid_argument & e, e.what(), "Wrong operator on 4");
           TS_ASSERT_THROWS_EQUALS(eval("tru"), const std::invalid_argument & e, e.what(), "Wrong operator on 3");
           TS_ASSERT_THROWS_EQUALS(eval("no"), const std::invalid_argument & e, e.what(), "Wrong operator on 2");
           TS_ASSERT_THROWS_EQUALS(eval("\"rehwaaegw\" == \"ewas"), const std::invalid_argument & e, e.what(), "Expecting \" on 20");
           TS_ASSERT_THROWS_EQUALS(eval("\"rehwaaegw\"=\"ewas\""), const std::invalid_argument & e, e.what(), "Wrong operator on 11");
            TS_ASSERT_THROWS_EQUALS(eval("\"rehwaaegw\"="), const std::invalid_argument & e, e.what(), "Wrong operator = on 11");
       }
};